from django.db import models

# Create your models here.

class Devices(models.Model):
    device_name = models.CharField(max_length=200)
    device_magnification = models.CharField(max_length=200)
    device_fieldofview = models.CharField(max_length=200)
    device_range = models.CharField(max_length=200)

    def __unicode__(self):
        return self.device_name
