from tastypie.resources import ModelResource
from tastypie.constants import ALL
from models import Devices

class DeviceResource(ModelResource):
    class Meta:
        queryset = Devices.objects.all()
        resource_name = 'devices'
        filtering = { 
            "device_name" : ALL,
        }

