# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Devices'
        db.create_table(u'myapp_devices', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('device_magnification', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('device_fieldofview', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('device_range', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'myapp', ['Devices'])


    def backwards(self, orm):
        # Deleting model 'Devices'
        db.delete_table(u'myapp_devices')


    models = {
        u'myapp.devices': {
            'Meta': {'object_name': 'Devices'},
            'device_fieldofview': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'device_magnification': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'device_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'device_range': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['myapp']