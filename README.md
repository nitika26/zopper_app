Download installReq

Then in terminal, run the following commands : 

sh installReq
source virt_env/virt1/bin/activate

cd zopper_app

pip install -r requirements.txt

python manage.py syncdb

python manage.py schemamigration myapp --auto

python manage.py migrate myapp
python manage.py migrate tastypie

python manage.py runserver

The app will be on the local server.

Firstly run, http://localserver:port/loaddb