from django import forms
from myapp.models import Devices


class DeviceForm(forms.ModelForm):
	class Meta:
	        model = Devices
	        fields = ('device_name','device_magnification','device_fieldofview','device_range')

