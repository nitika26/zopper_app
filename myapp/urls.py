from django.conf.urls import patterns, url
from myapp import views
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url('^$',TemplateView.as_view(template_name='index.html')),
    url(r'^adddevice/adddevicehelp/$', views.adddevicehelp, name='adddevicehelp'),
    url(r'^adddevice/$', TemplateView.as_view(template_name='addDevice.html')),
    url(r'^viewdevice/$', TemplateView.as_view(template_name='viewDevice.html')),
    url(r'^deletedevice/$', views.deletedevice, name='deletedevice'),
    url(r'^editdevice/$', TemplateView.as_view(template_name='editDevice.html')),
    url(r'^editdevicehelp/$', views.editdevicehelp, name='editdevicehelp'),
    url(r'^loaddb/$', views.loaddb, name='loaddb'),
) 

