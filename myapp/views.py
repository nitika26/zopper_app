from django.http import HttpResponse
import json
import urllib2
from myapp.models import Devices
from django.views.decorators.csrf import csrf_exempt
from myapp.forms import DeviceForm
from django.utils.html import escape
from django.http import HttpResponseRedirect
import ssl

def loaddb(request):

    url = "https://data.gov.in/node/88523/datastore/export/json"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    response = urllib2.urlopen(url,context=gcontext)
    page = response.read()
    d = json.loads(page)
    #return HttpResponse(page)
    for i in range(1,len(d["data"])):
	p = Devices.objects.create(device_name=d["data"][i][0],device_magnification=d["data"][i][1],device_fieldofview=d["data"][i][2],device_range=d["data"][i][3])
    return HttpResponse("Created objects")

@csrf_exempt
def adddevicehelp(request):
    var = {
        'result': 'failure',
	'error_code': '112',
    }
    if request.method == 'POST':

	device_form = DeviceForm(data=request.POST)

       	if device_form.is_valid():
		pd = device_form.save()	
		var = {
		    'result': 'success',
		}
		return HttpResponseRedirect(request.build_absolute_uri("/"))

    # RETURN ERROR PAGE
    data = json.dumps(var)
    return HttpResponse(data, mimetype='application/json')


@csrf_exempt
def deletedevice(request):

    var = {
	'result': 'failure',
	'error_code': '112',
    }
    if request.method == 'GET':
        device = Devices.objects.get(id=request.GET['id'])
	if device:
		device.delete()
		#Devices.objects.filter(id=request.GET['id']).delele()
		var = {
		    'result': 'success',
		}
		return HttpResponseRedirect(request.build_absolute_uri("/"))
    # RETURN ERROR PAGE
    data = json.dumps(var)
    return HttpResponse(data, mimetype='application/json')

@csrf_exempt
def editdevicehelp(request):
    var = {
        'result': 'failure',
	'error_code': '112',
    }
    if request.method == 'POST':
        device = Devices.objects.get(id=request.GET['id'])
	device_form = DeviceForm(data=request.POST)

       	if device_form.is_valid() and device:
	    device.device_name = request.POST['device_name']
	    device.device_magnification = request.POST['device_magnification']
	    device.device_fieldofview = request.POST['device_fieldofview']
	    device.device_range = request.POST['device_range']
	    device.save()
	    var = {
		'result': 'success',
            }
	    return HttpResponseRedirect(request.build_absolute_uri("/"))

    # RETURN ERROR PAGE
    data = json.dumps(var)
    return HttpResponse(data, mimetype='application/json')
 




