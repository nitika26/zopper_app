from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.conf import settings
from django.conf.urls.static import static

from tastypie.api import Api
from myapp.api import DeviceResource

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

device_resource = DeviceResource()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'zopper_app.views.home', name='home'),
    # url(r'^zopper_app/', include('zopper_app.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^myapp/', include('myapp.urls')),
    url('^$',RedirectView.as_view(url='/myapp/')),
    url(r'^api/',include(device_resource.urls)),
    url(r'^admin/', include(admin.site.urls)),
)


